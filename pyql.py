#Import libraries
import MySQLdb
import re
import subprocess
import random
import string



#Define database connection
db = MySQLdb.connect(host="localhost", port=3306, user="username", passwd="password", db="dbname")
cursor = db.cursor()


# Get users that have not been accepted
cursor.execute("SELECT uid FROM users_field_data WHERE status = 0")


#Initialise some variables and lists
count = 0 #counter
aList = list() #list of userids
xapi = list() #list of api keyds
row = cursor.fetchone() # defines row fetched
reg = 0 
sout = "0"



while row is not None: #take mysql output to list
	#print(row) 
        row = cursor.fetchone()
	userx = str(row)
	userx = re.sub(r'[(|)|\'|,|L]',r'',userx)
	#print userx #nice to have for debugging
	aList.append (userx); #append user id to list


	
n = len(aList) #get how many users are waiting for acceptance
n = n-1 # subract one
#print aList[0:1] #Nice for debugging
#print n #nice for debugging

if n == 0: # simple fix to make sure there is only one user processed each run
	x = 0
else:
	x = 1 # even if there are multiple users waiting, the program will behave like there is only one

#print x #Nice for debugging 

#exit(0) #Nice for debugging


# Fetch API info from user id

for item in aList[0:x]:
	#print item #yubb, even more debugging points
	cursor.execute("SELECT `field_keyid_value` FROM `user__field_keyid` WHERE `entity_id` = %s",(item)) #fetch keyid (adjust table name to yours)
	keyid = str(cursor.fetchone()) # write keyid to variable
	cursor.execute("SELECT `field_vcode_value` FROM `user__field_vcode` WHERE `entity_id` = %s",(item)) #fetch key code (adjust table name to yours)
	vcode = str(cursor.fetchone())  # write key code to variable
        
	
	#Fetch email info. Feature I didn't end up using, but decided not to delete it
	#cursor.execute("SELECT `mail` FROM `users_field_data` WHERE `uid` = %s",(item))
	#addr = str(cursor.fetchone()) 


	#Clean up api info
	vcode = re.sub(r'[(|)|\'|,|^]',r'',vcode) #remove special characters from api key code
        keyid = re.sub(r'[(|)|\'|,|L]',r'',keyid) #remove special characters from api key id
	#print vcode #just for debugging 
	#print keyid #just for debugging

	#Send API info to second script
	kol = "exo =subprocess.Popen(['python', '/root/ali.py', '%s', '%s'], stdout=subprocess.PIPE)" % (keyid, vcode) #define execution of api script

	exec kol #execute api call
        (out, err) = exo.communicate() # define output of second script as variables
        #print out #oh how, I havent seen a commented out debugging line before
	#print exo #and here is another one

	sout = str(out) #defines sout as the string of the output of the api caler	
	#print "this is sout"      #---
	#print sout                #   --- bugspray



if "InsertyourallianceID" in sout: #If the output of the second program contains your alliance id, execute the following code
	#print "user %s in alliance" %(addr) debug 
  	
	item = int(item) #define the user id as an interger
        addr = re.sub(r'[(|)|\'|,|^]',r'',addr) #clean up the email addres .... that doesn't get used

	#Unblocking the user
        dru2 = "exo =subprocess.call(['drush', '-r', '/var/www/html/drupal-8.0.5', 'uublk', '%s'])" % (item) #define the execution of drush to unblock a user
	exec dru2 #execute the drush command




else: #If the user isn't in the alliance, he probibly should be allowed
	#print "not in alliance" #I'll let you guess what this line is

        dru3 = "exo =subprocess.call(['drush', '-y', '-r', '/var/www/html/drupal-8.0.5', 'user-cancel', '%s'])" % (item) #define the cancelation of a user
	exec dru3 #execute the user deletion


db.close() #close the database connection
