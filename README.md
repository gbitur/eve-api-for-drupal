# EvE API Drupal authenticator #




### About the EvE API drupal authenticator ###

* What this is and isn't
* Prerequisites 
* how to setup
* This is free ?
* This isn't working, what do I do ?
* Flowchart

### What this is and isn't ###

#### What this is: 

* A simple method for a tech savvy person to implement an automatic method of allowing only alliance members on a drupal website
* A jumping off point for programmers to have fun
* A piece of dirty code requiring a lot of prerequisites to work properly

####What this isn't:

* A perfect plug and play solution for someone without sql/python experience to install in 15 minutes


### Prerequisites ###

* Full, real VPS access
* Clean drupal install (only tested with drupal 8)
* Drupal registration field named field_keyid and field_vcode yourwebsite.tld/admin/config/people/accounts/fields
* drush (command line tool to manage drupal)
* Installed and working copy of the EvE API python project https://github.com/ntt/eveapi

### How to set up ###

* Configure drupal for everyone can register, but need admin approoval
* Install and configure the EvE API project from the git source
* Create drupal fields for api info
* Fetch this project into a folder outside your webserver (you don't want these files accesable from the outside)
* move ali.py into the EvE API project folder
* create a symlink to ali.py to a fol
* try the EvE api script "python /symlinkto/ali.py keyid vcode"
* It returns alliance ID's ? move on. It doesn't ? debug it before moving on
* Edit your pyql.py script, set your database settings, location of ali.py symlink, location of drupal install and alliance ID
* make sure your mysql fields are the same in your database and script
* Create some user accounts
* run pyql.py "python /locationof/pyql.py"
* If it returns "userd accepted" or "user deleted" when ran, it works :D
* It didn't ? debug it, that's what all those debug lines are there for 
* It works ? great !. create a crontab to run it once a minute

### This is free ? ###

* Yeah, open source is awesome :)
* I would like to say thanks, do you accept ISK ? Yes I do :D Lalia Ichinumi is one of my alts :) 
* I used your code to develop something else ... Awesome, let me know, I'd like to see it

### This isn't working, what do I do ? ###

I don't really plan on doing customer support for this project.

### Flowchart ###

![picture](http://i.imgur.com/um7P6rk.jpg)